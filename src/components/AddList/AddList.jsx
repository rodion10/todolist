import React from "react";
import List from "../List/List.jsx";
import "./AddList.scss";
import axios from 'axios';
import Badge from "../Badge/Badge.jsx";
import closeSvg from "../../assets/img/close.svg";


const AddList = ({ colors, onAdd }) => {
  const [visablePopup, setVisablePopup] = React.useState(false);
  const [seletedColor, selectColor] = React.useState(3);
  const [inputValue, setInputValue] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  
    React.useEffect(()=>{
      if (Array.isArray(colors)) {
        selectColor(colors[0].id);
      }
    }, [colors]);

  const onClose = ()=> {
    setVisablePopup(false);
    setInputValue("");
    selectColor(colors[0].id)
  }

  const addList = () => {
    if (!inputValue) {
      alert("Введите название списка");
      return;
    }
    setIsLoading(true);
    axios
      .post('http://localhost:3001/lists', {
        name: inputValue,
        colorId: seletedColor
      })
      .then(({ data }) => {
        const color = colors.filter(c => c.id === seletedColor)[0].name;
        const listObj = { ...data, color: { name: color } };
        onAdd(listObj);
        onClose();
        
      })
      .catch(() => {
        alert('Ошибка при добавлении списка!');
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  return (
    <div className="add-list">
      <List
        onClick={() => setVisablePopup(!visablePopup)}
        items={[
          {
            className: "list__add-button",
            icon: (
              <svg
                width="12"
                height="12"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M8 1V15"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M1 8H15"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            ),
            name: "Добавить список",
          },
        ]}
      />
      {visablePopup && (
        <div className="add-list__popup">
          <img
            onClick={() => onClose()}
            src={closeSvg}
            alt="closr button"
            className="add-list__popup-close-btn"
          />
          <input
            className="field"
            type="text"
            placeholder="Название списка"
            value={inputValue}
            onChange={(e) => setInputValue(e.target.value)}
          />
          <div className="add-list__popup-colors">
            {colors.map((color) => (
              <Badge
                key={color.id}
                onClick={() => selectColor(color.id)}
                color={color.name}
                className={seletedColor === color.id && "active"}
              />
            ))}
          </div>{" "}
          <button onClick={addList} className="button">
          {isLoading ? 'Добавление...' : 'Добавить'}
          </button>
        </div>
      )}
    </div>
  );
};
export default AddList;
