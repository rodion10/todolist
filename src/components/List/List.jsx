import React from "react";
import "./List.scss";
import classNames from "classnames";
import Badge from "../Badge/Badge";
import removeSvg from "./../../assets/img/remove.svg";
import axios from "axios";

const List = ({
  items,
  isRemovable,
  onClick,
  onRemove,
  onClickItem,
  activeItem,
}) => {
  const removeList = (item) => {
    if (window.confirm("Вы действительно хотите удалить список? "))
      axios.delete("http://localhost:3001/lists/" + item.id).then(() => {
        onRemove(item.id);
      })
      .catch(() => {
        alert("Ошибка при удалении списка!");
      })
  };

  return (
    <ul onClick={onClick} className="list">
      {items.map((item) => (
        <li
          key={item.name}
          className={classNames(item.className, {
            active: item.active
              ? item.active
              : activeItem && activeItem.id === item.id
          })}
          onClick={onClickItem ? () => onClickItem(item) : null}
        >
          <i>{item.icon ? item.icon : <Badge color={item.color.name} />}</i>
          <span>
            {item.name}
            {item.tasks && ` (${item.tasks.length})`}
          </span>
          {isRemovable && (
            <img
              className="list__remove-icon"
              src={removeSvg}
              alt="Remove icon"
              onClick={() => removeList(item)}
            />
          )}
          {onRemove}
        </li>
      ))}
    </ul>
  );
};

export default List;
